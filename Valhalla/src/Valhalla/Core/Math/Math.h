#pragma once
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/quaternion.hpp>
#include <glm/glm.hpp>
#include <DirectXMath.h>

namespace DirectX
{
	typedef XMFLOAT4 XMQUAT;
}